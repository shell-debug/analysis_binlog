#!/bin/bash
# File Name: ../function/show_help.sh
# Author: moshan
# mail: mo_shan@yeah.net
# Created Time: 2019-04-15 16:44:16
# Function: 
#########################################################################
work_dir=/data/git/analysis_binlog
function f_show_help()
{
	echo
	echo -e "\033[33m"
	echo "Usage: bash ${script_name} [OPTION]..."
	echo
	echo "--type=value or -t=value          The value=detail | simple"
	echo "                                  For example: --type=detail,-t=detail,-t=simple,-t=simple,"
	echo "                                  The \"detail\": The results displayed are more detailed, but also take more time."
	echo "                                  The \"simple\": The results shown are simple, but save time"
	echo "                                  The default value is \"simple\". "
	echo
	echo "--binlog-dir or -bdir             Specify a directory for the binlog dir."
	echo "                                  For example: --binlog-dir=/mysql_binlog_dir,-bdir=/mysql_binlog_dir"
	echo "                                  If the input is a relative path, it will be automatically modified to an absolute path."
	echo "                                  The default value is \"Current path\". "
	echo
	echo "--binlog-file or -bfile           Specify a file for the binlog file, multiple files separated by \",\"."
	echo "                                  For example: --binlog-file=/path/mysql_binlog_file,-bfile=/path/mysql_binlog_file"
	echo "                                               --b-file=/path/mysql_binlog_file1,/path/mysql_binlog_file1"
	echo "                                  If the input is a relative path, it will be automatically modified to an absolute path."
	echo "                                  If this parameter is used, the \"--binlog-dir or -bdir\" parameter will be invalid."
	echo
	echo "--sort or -s                      Sort the results for \"INSERT | UPDATE | DELETE | Total\""
	echo "                                  The value=insert | update | delete | total"
	echo "                                  The default value is \"insert\"."
	echo
	echo "--threads or -w                   Decompress/compress the number of concurrent. For example:--threads=8"
	echo "                                  This parameter works only when there are multiple files."
	echo "                                  If you use this parameter, specify a valid integer, and the default value is \"1\"."
	echo
	echo "--start-datetime or -stime        Start reading the binlog at first event having a datetime equal or posterior to the argument;"
	echo "                                  The argument must be a date and time in the local time zone,"
	echo "                                  in any format accepted by the MySQL server for DATETIME and TIMESTAMP types,"
	echo "                                  for example: -stime=\"2019-04-28 11:25:56\" (you should probably use quotes for your shell to set it properly).. "
	echo
	echo "--stop-datetime or -etime         Stop reading the binlog at first event having a datetime equal or posterior to the argument;"
	echo "                                  The argument must be a date and time in the local time zone,"
	echo "                                  in any format accepted by the MySQL server for DATETIME and TIMESTAMP types,"
	echo "                                  for example: -etime=\"2019-04-28 11:25:56\" (you should probably use quotes for your shell to set it properly)."
	echo "                                  Applies to the first binlog passed on the command line."
	echo
	echo "--start-position or -spos         Start reading the binlog at position N(Integer). "
	echo "                                  Applies to the first binlog passed on the command line."
	echo "                                  For example: --start-position=154 or -spos=154"
	echo
	echo "--stop-position or -epos          Stop reading the binlog at position N(Integer). "
	echo "                                  Applies to the last binlog passed on the command line."
	echo "                                  For example: --stop-position=154 or -epos=154"
	echo
	echo "--database or -d                  List entries for just this database (local log only). "
	echo "                                  For example: --database=db_name or -d=db_name"
	echo
	echo "--binlog2sql or -sql              Convert binlog file to sql. At this time, the \"--type or -t, --sort or -s\" option will be invalid."
	echo "                                  For example: --binlog2sql or -sql"
	echo
	echo "--save-way or -sw                 The value=table | file. How to save the analysis results and this option needs to be used with the a option."
	echo "                                  For example: --save-way=file or -sw=table, the default value is \"file\"."
	echo "                                  file : Save the results in a file."
	echo "                                  table: Save the results in different files according to the table name. These file names are called \"db.table\"."
	echo
	echo "--help or -h                      Display this help and exit."
	echo -e "\033[0m"
	echo
}

